from math import copysign, fabs, floor, isfinite, modf, log2, ceil


def float_to_bin_fixed(f):
    if not isfinite(f):
        return repr(f)  # inf nan

    sign = '-' * (copysign(1.0, f) < 0)
    frac, fint = modf(fabs(f))  # split on fractional, integer parts
    n, d = frac.as_integer_ratio()  # frac = numerator / denominator
    assert d & (d - 1) == 0  # power of two
    return f'{sign}{floor(fint):b}.{n:0{d.bit_length() - 1}b}'


class Shennon():
    def __init__(self, alphabet, p_list):
        self.alphabet = []
        self.coded_alphabet = {}
        for i in range(len(alphabet)):
            self.alphabet.append([alphabet[i], p_list[i]])
        self.alphabet.sort(key = lambda x: x[1], reverse=True)
        self.add_p_sum()

    def add_p_sum(self):
        sum = 0
        for i in range(len(self.alphabet)):
            self.alphabet[i].append(sum)
            sum += self.alphabet[i][1]
        self.after_point_amount()

    def after_point_amount(self):
        for i in self.alphabet:
            i.append(ceil(log2(1/i[1])))
        self.convert_to_bit()

    def convert_to_bit(self):
        for i in self.alphabet:
            bit = float_to_bin_fixed(i[2])
            if bit == "0.0":
                bit = "0.000000000000000000000"
            bit = bit.split(".")
            bit = bit[1][:i[3]]
            i.append(bit)
        self.create_coded_alphabet()

    def create_coded_alphabet(self):
        for i in self.alphabet:
            self.coded_alphabet[i[0]] = i[4]

    def code_word(self, word):
        print(self.coded_alphabet)
        end_message = ""
        for i in word:
            try:
                end_message += self.coded_alphabet[i]
            except:
                return "Сообщение содержит недопустимые символы"
        return end_message


if __name__ == '__main__':
    with open('alphabet2.txt', 'r') as f:
        string = f.readline().split(" ")
        string = [line.rstrip() for line in string]

        prob = f.readline().split(" ")
        prob = [float(i) for i in prob]

    s = Shennon(string, prob)
    print(s.code_word("adc"))
