import re

class Decoder:
    def __init__(self, data, alphabet, line):
        self.string = data
        self.data = []
        self.alphabet = sorted(alphabet)
        self.length = len(data)
        self.word = ""
        self.line = line
        self.result = ""

    def book_decode(self):
        count = 0
        while 2 ** count < len(self.alphabet):
            count += 1
        chunks = re.findall('.{%s}' % count, self.string)
        for i in range(self.length//count):
            self.data.append(int(chunks[i], 2))
        self.length = len(chunks)
        for i in range(self.length):
            now = self.alphabet[int(self.data[i])]
            self.word += now
            self.alphabet.insert(0, self.alphabet[int(self.data[i])])
            self.alphabet.pop(int(self.data[i])+1)

    def bu_decode(self):
        self.book_decode()
        self.line = int(f"0b{self.line}", 2)
        g = [list(self.word) for _ in range(self.length)]
        c = [[] for _ in range(self.length)]
        if self.length > 1:
            for i in range(1, self.length):
                c[i] = sorted(g[i - 1])
                for j in range(self.length):
                    g[i][j] += c[i][j]
            result_dict = sorted(g[-1])
            self.result = result_dict[self.line]
        else:
            self.result = self.word


if __name__ == "__main__":
    alphabet = open('alphabet.txt', 'r').read().split(" ")
    print(alphabet)
    coding_result = input("Введите слово для декодирования: ")
    coding_line = input("Введите номер линии: ")

    d = Decoder(coding_result, alphabet, int(coding_line))
    d.bu_decode()
    print(f"Было закодировано слово: {d.result}")
